<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @/throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            try{
                $this->db = new PDO('mysql:host=localhost;dbname=part1.2;charset=utf8mb4', 'root', '');
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }   
		}

          
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {   
		$booklist = array();
        try{
                $getBooks = $this->db->query("SELECT * FROM book ORDER BY id");
                $rows = $getBooks->fetchAll();
		
		foreach ($rows as $book) {  
			$booklist[] = new Book($book['title'], $book['author'], $book['description'], $book['id']);
		}
            } catch (PDOException $e) {
                echo 'Failed to get booklist' . $e->getMessage();
            }
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        if(!filter_var($id, FILTER_VALIDATE_INT)){
			return null;
		}
		$book = null;
        try{
            $stmt = $this->db->prepare("SELECT id, title, author, description FROM book WHERE id=?");
            $stmt->bindParam(1, $id, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
            } catch (PDOException $e) {
                echo 'Failed to get book' . $e->getMessage();
            }   

        if($book == FALSE)	{
			return null;
		}
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        try{
            if ($book->title != null && $book->title != '' && $book->author != null && $book->author != ''){
                $prep = $this->db->prepare('INSERT INTO Book(title, author, description) VALUES(:title, :author, :description)');
                $prep->bindParam(':title', $book->title, PDO::PARAM_STR, 12);
                $prep->bindParam(':author', $book->author, PDO::PARAM_STR, 12);
                $prep->bindParam(':description', $book->description, PDO::PARAM_STR, 12);
                $prep->execute();   
            }
        } catch (PDOException $e) {
                echo 'Failed adding book: ' . $e->getMessage();
            }
    }
        
    
    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
            $prep = $this->db->prepare('UPDATE book SET title = :title, author = :author, description = :description WHERE id = :id');
            $prep->bindParam(':title', $book->title, PDO::PARAM_STR, 12);
            $prep->bindParam(':author', $book->author, PDO::PARAM_STR, 12);
            $prep->bindParam(':description', $book->description, PDO::PARAM_STR, 12);
            $prep->bindParam(':id', $book->id, PDO::PARAM_INT);
            $prep->execute();
			
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if (getBookById($id) != null){
            $prep = $this->db->prepare('DELETE FROM book WHERE id = :id');
            $prep->bindParam(':id', $id, PDO::PARAM_INT);
            $prep->execute();
        )
    }
	
}

?>